import React from "react";
import { StyleSheet } from "react-native";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import List from "./src/components/list";
import itemDetails from "./src/components/itemDetails";

const Stack = createStackNavigator();

export default function App() {
  return (

    <NavigationContainer>
      <Stack.Navigator>
          <Stack.Screen name="List" component={List} />
          <Stack.Screen name="itemDetails" component={itemDetails} />
       </Stack.Navigator>
     </NavigationContainer>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
  },
});

{/* <View style={styles.container}>
<List />
</View> */}