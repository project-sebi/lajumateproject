import { StyleSheet } from "react-native";
const styles = StyleSheet.create({
  image: {
    width: "100%",
    height: 200,
  },

  price: {
    fontSize: 20,
    color: "#009688",
    fontWeight: "bold",
    marginTop: 10,
    marginLeft: 10,
  },

  title: {
    marginLeft: 10,
    marginTop: 10,
    width: "65%",
    fontWeight: "600",
  },

  description: {
    marginLeft: 10,
    marginTop: 10,
  },

  block: {
    flexDirection: "row",
  },

  location: {
    marginLeft: 10,
    marginTop: 10,
    color: "#b9b9b9",
  },

  data: {
    marginLeft: "auto",
    marginTop: 10,
    marginRight: 10,
    color: "#b9b9b9",
  },

  bottom: {
    marginTop: 10,
    height: 15,
    backgroundColor: "#cbcbcb"
  },

  header: {
    height: 70,
    alignItems: "center",
  },

  headerTitle: {
    color: "#fff",
    fontSize: 23,
    marginTop: 20
  }
});

export { styles };
