import React from "react";
import { StyleSheet, Text, View } from "react-native";
import { LinearGradient } from "expo-linear-gradient";
import CardDetails from "./cardDetails";
import { styles } from '../css/cards';

const itemDetails = () => {
  return (
    <View>
      <LinearGradient colors={["#00796b", "#009688"]} style={styles.header}>
        <Text style={styles.headerTitle}>Detalii anunt</Text>
      </LinearGradient>
    <CardDetails  />
    </View>
  );
};



export default itemDetails;
