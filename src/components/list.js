import React from "react";
import { StyleSheet, View, Image, ScrollView, Text } from "react-native";
import { LinearGradient } from "expo-linear-gradient";
import { getToken, getDetails, getItems } from "../api/token";
import CardList from "./cardList";
// import axios from "axios";

const list = () => {
  let [token, setToken] = React.useState("");
  let [list, setList] = React.useState([]);
  let [details, setDetails] = React.useState({});

  React.useEffect(() => {
    async function fetchToken() {
      let response = await fetch(
        "https://lajumate.ro/test-web/sebastian/create-token",
        {
          method: "post",
          headers: {
            "public-key":
              "VXGNq3GcR73gNJ2FLN2cMZwHPPujy71yrveK0scORS7KzoXWVyjXjh6vzZ1spvzg",
          },
          params: {
            loggedIn: 1,
            version: 2,
            source: "web",
          },
        }
      );
      response = await response.json();
      setToken(response);
      // console.log(response);
      fetchList(response.token);
      fetchDetails(response.token);
    }

    const fetchList = async (token) => {
      let responseList = await fetch(
        "https://lajumate.ro/test-web/sebastian/search-items?loggedIn=1&version=2&source=web",
        {
          method: "post",
          headers: {
            "public-key":
              "VXGNq3GcR73gNJ2FLN2cMZwHPPujy71yrveK0scORS7KzoXWVyjXjh6vzZ1spvzg",
            token,
          },
        }
      );
      responseList = await responseList.text();
      setList(JSON.parse(responseList).ads);
      // console.log(JSON.parse(responseList).ads);
    };

    const id = 4253803;
    const fetchDetails = async (token) => {
      let response = await fetch(`https://lajumate.ro/test-web/sebastian/item-details/${id}?loggedIn=1&version=2&source=web`, {
        'method': 'post',
        headers: {
          'public-key': 'VXGNq3GcR73gNJ2FLN2cMZwHPPujy71yrveK0scORS7KzoXWVyjXjh6vzZ1spvzg',
          token,
        },
        // params: {
        //   'loggedIn': 1,
        //   'version': 2,
        //   'source': 'web'
        // }
      })
      .then(res => res.text())
      setDetails(response)
      console.log(response);

    }

    fetchToken();
  }, []);
// fetchDetails();

  return (
    <ScrollView>
      <View>
        <LinearGradient colors={["#00796b", "#009688"]} style={styles.header}>
          <Image
            style={styles.logo}
            source={require("../images/logo-ljm.png")}
          />
        </LinearGradient>
        <View>
          {list.map((object, key) => {
            return (
              // <Text>{list}</Text>
              <CardList object={object} />
            );
          })}
        </View>
      </View>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  header: {
    height: 70,
    alignItems: "center",
  },

  logo: {
    width: 210,
    marginTop: 20,
  },

  pageTitle: {
    color: "#fff",
    textAlign: "center",
    paddingTop: 25,
    fontSize: 20,
  },
});

export default list;
