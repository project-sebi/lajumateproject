import React, { useState } from "react";
import { Text, View, Image, ScrollView } from "react-native";
import { styles } from '../css/cards';

const cardDetails = () => {
    // let [product, setProduct] = useState([])

  return (
    <ScrollView>
      <View>
        <Image style={styles.image} source={require("../images/car.jpg")}
        />
        <Text style={styles.price}>79.150 EUR</Text>
        <Text style={styles.title}>asd</Text>
        <View style={styles.block}>
          <Text style={styles.location}>Bucuresti, IF</Text>
          <Text style={styles.data}>26 sep.</Text>
        </View>
        <View style={styles.bottom}></View>
        <Text style={styles.description}>
          Adusa in tara in 2017 dar foarte putin rulata. Primele Numere rosii au
          fost scoase in urma cu 2 luni. Revizie motor facuta recent Geamuri
          electrice ...
        </Text>
      </View>
    </ScrollView>
  );
};



export default cardDetails;
