import React, { useState } from "react";
import { Text, View, Image, ScrollView } from "react-native";
import { useNavigation } from "@react-navigation/native";
import { styles } from "../css/cards";

const cardList = (props) => {
  // let [product, setProduct] = useState([])
  const navigation = useNavigation();
  return (
    <View>
      <Image
        style={(props.thumbHeight, props.thumbWidth)}
        source={{uri: props.firstImage}}
      />
      <Text
        style={styles.price}
        onPress={() => navigation.navigate("itemDetails")}
      >
        {props.object.price} {props.object.currency}
      </Text>
      <Text
        style={styles.title}
        onPress={() => navigation.navigate("itemDetails")}
      >
        {props.object.title}
      </Text>
      <Text
        style={styles.description}
        onPress={() => navigation.navigate("itemDetails")}
      >
        Lorem Ipsum is simply dummy text of the printing and typesetting
        industry. Lorem Ipsum has been the industry's standard dummy text ever
        since the 1500s, when an unknown printer took a galley of type and
        scrambled it to make a type specimen book.
      </Text>
      <View style={styles.block}>
        <Text style={styles.location}>{props.object.location}</Text>
        <Text style={styles.data}>{props.object.date}</Text>
      </View>
      <View style={styles.bottom}></View>
    </View>
  );
};

export default cardList;
